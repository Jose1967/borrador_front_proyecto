function app() {

    //const formCursos = document.querySelector('#formCursos')
    const oDatos = {}
    //const inNombre =  document.querySelector('#nombre')
    //const url = 'https://jsonplaceholder.typicode.com/todos'
    //console.dir(formCursos)

    formCursos.addEventListener('submit', enviar)
    //inNombre.addEventListener('change', cambio)
    //inNombre.addEventListener('input', cambio)
    //document.querySelector('#curso').addEventListener('change', cambio)

    function cambio(ev) {
        console.log(ev.target.value)
    }

    function enviar(ev) {
        ev.preventDefault()
        console.log('Enviando')
        oDatos.nombre = document.querySelector('#nombre').value
        oDatos.apellidos = document.querySelector('#apellidos').value
        oDatos.domicilio = document.querySelector('#domicilio').value
        oDatos.dni = document.querySelector('#dni').value
        oDatos.telefono = document.querySelector('#telefono').value
        oDatos.email = document.querySelector('#email').value
        oDatos.passw = document.querySelector('#passw').value
        oDatos.tratamiento = getRadio('tratamiento')
        console.dir(oDatos)
    }

    function getRadio(name){
        const aRadio = document.querySelectorAll(`[name="${name}"]`)
        console.dir(aRadio)
        for (let i = 0; i < aRadio.length; i++) {
            const item = aRadio[i];
            if (item.checked) {
                return item.value
            }
        }
    }

    zfill(number, width) {
    var numberOutput = Math.abs(number);
    var length = number.toString().length;
    var zero = "0";  //String de cero
    if (width <= length) {
        if (number < 0) {
             return ("-" + numberOutput.toString());
        } else {
             return numberOutput.toString();
        }
    } else {
        if (number < 0) {
            return ("-" + (zero.repeat(width - length)) + numberOutput.toString());
        } else {
            return ((zero.repeat(width - length)) + numberOutput.toString());
        }
    }
}



}

document.addEventListener('DOMContentLoaded', app )
